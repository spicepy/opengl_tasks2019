add_compile_definitions(GLM_ENABLE_EXPERIMENTAL)

include_directories(common)
include_directories(mazekit)
include_directories(mazekit/meshes)

set(HEADER_FILES
	common/Application.hpp
	common/Camera.hpp
	common/Common.h
	common/ConditionalRender.h
	common/DebugOutput.h
	common/Framebuffer.hpp
	common/LightInfo.hpp
	common/Mesh.hpp
	common/QueryObject.h
	common/ShaderProgram.hpp
	common/SkinnedMesh.h
	common/Texture.hpp
	common/Utils.h
	
	mazekit/maze.hpp
	mazekit/maze_application.hpp
	mazekit/maze_camera_mover.hpp
	
	mazekit/meshes/parallelepiped.hpp
)

set(SRC_FILES
	common/Application.cpp
	common/Camera.cpp
	common/ConditionalRender.cpp
	common/DebugOutput.cpp
	common/Framebuffer.cpp
	common/Mesh.cpp
	common/QueryObject.cpp
	common/ShaderProgram.cpp
	common/SkinnedMesh.cpp
	common/Texture.cpp
	common/Utils.cpp
	
	mazekit/main.cpp
	mazekit/maze.cpp
	mazekit/maze_application.cpp
	mazekit/maze_camera_mover.cpp
	
	mazekit/meshes/parallelepiped.cpp
)

MAKE_OPENGL_TASK(597Tkachenko 1 "${SRC_FILES}")