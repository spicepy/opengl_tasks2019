#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>

class SampleApplication : public Application
{
public:
    MeshPtr _relief;

    ShaderProgramPtr _shader;


    void makeScene() override
    {
        Application::makeScene();

        //=========================================================
        //Создание и загрузка мешей

        _relief = makeRelief();
        _relief->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(-20.0f, -20.0f, 0.0f)));

        _cameraMover = std::make_shared<FreeCameraMover>();


        //=========================================================
        //Инициализация шейдеров

        _shader = std::make_shared<ShaderProgram>("596ShirokovaData1/color.vert", "596ShirokovaData1/color.frag");

    }


    void draw() override
    {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        _shader->setMat4Uniform("modelMatrix", _relief->modelMatrix());

        _relief->draw();

    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}