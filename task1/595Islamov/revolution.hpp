#include "./common/Mesh.hpp"
#include <glm/simd/geometric.h>
#include <vector>
#include <algorithm>


glm::vec3 normal(float x, float y, float z, float x_n, float y_n, float z_n) {
    glm::vec3 start = glm::vec3(x_n, y_n, z_n);
    glm::vec3 end = glm::vec3(x, y, z);
    return normalize(glm::vec3(start.x - end.x, start.y - end.y, start.z - end.z));
}

MeshPtr generateSurface(float height, float radius, unsigned int N) {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    for (size_t i = 0; i < 2 * N; ++i) {

        float curr_r = radius * i * (2 * N - i) / N / N;
        float next_r = radius * (i + 1) * (2 * N - i - 1) / N / N;

        float curr_h = height * i / 2 / N;
        float next_h = height * (i + 1) / 2 / N;

        for (size_t j = 0; j < N; ++j) {

            float phi = 2 * glm::pi<float>() * j / N;
            float theta = 2 * glm::pi<float>() * (j + 1) / N;

            float z = (curr_h + next_h) / 2;

            vertices.emplace_back(curr_r * cos(phi), curr_r * sin(phi), curr_h);
            vertices.emplace_back(next_r * cos(theta), next_r * sin(theta), next_h);
            vertices.emplace_back(curr_r * cos(theta), curr_r * sin(theta), curr_h);

            normals.emplace_back(normal(curr_r * cos(phi), curr_r * sin(phi), curr_h, 0, 0, z));
            normals.emplace_back(normal(next_r * cos(theta), next_r * sin(theta), next_h, 0, 0, z));
            normals.emplace_back(normal(curr_r * cos(theta), curr_r * sin(theta), curr_h, 0, 0, z));

            vertices.emplace_back(curr_r * cos(phi), curr_r * sin(phi), curr_h);
            vertices.emplace_back(next_r * cos(theta), next_r * sin(theta), next_h);
            vertices.emplace_back(next_r * cos(phi), next_r * sin(phi), next_h);

            normals.emplace_back(normal(curr_r * cos(phi), curr_r * sin(phi), curr_h, 0, 0, z));
            normals.emplace_back(normal(next_r * cos(theta), next_r * sin(theta), next_h, 0, 0, z));
            normals.emplace_back(normal(next_r * cos(phi), next_r * sin(phi), next_h, 0, 0, z));

        }

    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());
    return mesh;
}