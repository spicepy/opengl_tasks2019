#pragma once

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <random>

class Maze
{
public:
    float startXPosition, startYPosition;

    Maze(std::string filepath);

    std::vector<bool>& operator[](int idx);
    const std::vector<bool>& operator[](int idx) const;

    unsigned long GetHeight() const;
    unsigned long GetWidth() const;

private:
    std::vector<std::vector<bool> > mazeCells;
};
