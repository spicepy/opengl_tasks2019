#include "Maze.hpp"

#include <assert.h>

Maze::Maze(std::string filepath)
{
    std::ifstream mazeFile(filepath.c_str());

    unsigned long height, width;
    mazeFile >> height >> width;
    mazeFile >> startXPosition >> startYPosition;

    mazeCells = std::vector<std::vector<bool> >(height, std::vector<bool>(width));
    char c;
    for (int i = 0; i < height; ++i)
    {
        for (int j = 0; j < width; ++j)
        {
            mazeFile >> c;
            mazeCells[i][j] = ( c == '#' );
        }
    }
    mazeFile.close();
}

std::vector<bool> &Maze::operator[](int idx)
{
    return mazeCells[idx];
}

const std::vector<bool> &Maze::operator[](int idx) const
{
    return mazeCells[idx];
}

unsigned long Maze::GetHeight() const
{
    return mazeCells.size();
}

unsigned long Maze::GetWidth() const
{
    return mazeCells[0].size();
}
