#include <glm/ext.hpp>

#include "common/Application.hpp"
#include "common/Camera.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"
#include "MazeCameraMover.hpp"

#include <iostream>
#include <fstream>
#include <vector>

class MazeApplication : public Application
{
public:
    MazeApplication(Maze &maze) : Application(std::make_shared<MazeCameraMover>(maze)),
                                  cameraActive(_cameraMover),
                                  cameraInactive(std::make_shared<MazeCameraMover>(maze, true))
    {
        localMaze = &maze;
    }

    ~MazeApplication() { }

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        mazeMesh = makeMaze(*localMaze);
        mazeMesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.5f)));
        //_faces.load();

        shader = std::make_shared<ShaderProgram>("597VinokurovData1/shader.vert",
                "597VinokurovData1/shader.frag");
    }

    void update() override
    {
        double dt = glfwGetTime() - _oldTime;

        glm::mat4 matrix = mazeMesh->modelMatrix();
        mazeMesh->setModelMatrix(matrix);

        Application::update();
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        shader->use();

        shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        shader->setMat4Uniform("modelMatrix", mazeMesh->modelMatrix());
        mazeMesh->draw();
    }

    void handleKey(int key, int scancode, int action, int mods)
    {
        if (action == GLFW_PRESS)
        {
            if (key == GLFW_KEY_C)
            {
                cameraActive.swap(cameraInactive);
                _cameraMover = cameraActive;
            }
        }
        Application::handleKey(key, scancode, action, mods);
    }

protected:
    MeshPtr mazeMesh;
    Maze* localMaze;
    ShaderProgramPtr shader;
    std::shared_ptr<CameraMover> cameraActive;
    std::shared_ptr<CameraMover> cameraInactive;
};

int main()
{
    Maze maze("597VinokurovData1/maze");
    MazeApplication app(maze);
    app.start();
}
