#pragma once

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>
#include <glm/ext.hpp>

#include "common/Camera.hpp"
#include "Maze.hpp"

#include <vector>
#include <iostream>

class MazeCameraMover : public CameraMover
{
public:
    MazeCameraMover(Maze &maze, bool free = false);

    void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) override;
    void handleMouseMove(GLFWwindow* window, double xpos, double ypos) override;
    void handleScroll(GLFWwindow* window, double xoffset, double yoffset) override;
    void update(GLFWwindow* window, double dt) override;

    void move(glm::vec3 pos);

protected:
    glm::vec3 _pos;
    glm::quat _rot;
    bool _free;

    double PreviousXPosition = 0.0;
    double PreviousYPosition = 0.0;

private:
    Maze &maze;
};
